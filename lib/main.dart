import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:soundpool/soundpool.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';

Soundpool _soundpool;
bool blueGreen = true;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  _soundpool = Soundpool();
  runApp(SimpleApp());
}

class SimpleApp extends StatefulWidget {
  SimpleApp({Key key}) : super(key: key);

  @override
  _SimpleAppState createState() => _SimpleAppState();
}

class _SimpleAppState extends State<SimpleApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Developer Day - SoundBoost',
      theme: ThemeData(
        primarySwatch: (blueGreen)?Colors.blue:Colors.green,
      ),
      home: MyHomePage(title: 'Developer Day - SoundBoost'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  Soundpool pool = Soundpool(streamType: StreamType.notification);

  Future<int> _soundId0;
  Future<int> _soundId1;
  Future<int> _soundId2;
  Future<int> _soundId3;
  Future<int> _soundId4;
  Future<int> _soundId5;
  Future<int> _soundId6;
  Future<int> _soundId7;
  Future<int> _soundId8;
  Future<int> _soundId9;

  double _hightwidth;
  double _radius;
  double _margin;
  double _iconsize;

  bool _button0pressed;
  bool _button1pressed;
  bool _button2pressed;
  bool _button3pressed;
  bool _button4pressed;
  bool _button5pressed;
  bool _button6pressed;
  bool _button7pressed;
  bool _button8pressed;
  bool _button9pressed;

  Color _bordercolor;
  double _br;

  void initState() {
    super.initState();
    _soundId0 = _loadSound(0);
    _soundId1 = _loadSound(1);
    _soundId2 = _loadSound(2);
    _soundId3 = _loadSound(3);
    _soundId4 = _loadSound(4);
    _soundId5 = _loadSound(5);
    _soundId6 = _loadSound(6);
    _soundId7 = _loadSound(7);
    _soundId8 = _loadSound(8);
    _soundId9 = _loadSound(9);

    _hightwidth = 140;
    _radius = _hightwidth / 2;
    _margin = _hightwidth / 3;
    _iconsize = _radius + _radius / 7;

    _br = (blueGreen)?5.0:0.0;
    _bordercolor = Colors.black;

    _button0pressed = false;
    _button1pressed = false;
    _button2pressed = false;
    _button3pressed = false;
    _button4pressed = false;
    _button5pressed = false;
    _button6pressed = false;
    _button7pressed = false;
    _button8pressed = false;
    _button9pressed = false;
  }

  setWidth(double _width, double _height) {
    setState(() {
      double _val = _width;
      if (_height < _width) {
        _val = _height;
      }
      print('smallest size:' + _val.toString());
      _hightwidth = _val / 5;
      _radius = _hightwidth / 2;
      _margin = _hightwidth / 3;
      _iconsize = _radius + _radius / 7;
    });
  }

  Future<int> _loadSound(int num) async {
    switch (num) {
      case 0:
        return await _soundpool
            .load(await rootBundle.load("lib/sounds/gong.mp3"));
      case 1:
        return await _soundpool
            .load(await rootBundle.load("lib/sounds/cartoon_ascend.mp3"));
      case 2:
        return await _soundpool
            .load(await rootBundle.load("lib/sounds/horror_stab.mp3"));
      case 3:
        return await _soundpool
            .load(await rootBundle.load("lib/sounds/ring_short.mp3"));
      case 4:
        return await _soundpool
            .load(await rootBundle.load("lib/sounds/horror_hit.mp3"));
      case 5:
        return await _soundpool
            .load(await rootBundle.load("lib/sounds/male_laugh.mp3"));
      case 6:
        return await _soundpool
            .load(await rootBundle.load("lib/sounds/phone_slash.mp3"));
      case 7:
        return await _soundpool
            .load(await rootBundle.load("lib/sounds/nature_thunder.mp3"));
      case 8:
        return await _soundpool
            .load(await rootBundle.load("lib/sounds/monkey.mp3"));
      case 9:
        return await _soundpool
            .load(await rootBundle.load("lib/sounds/police_siren.mp3"));
      default:
        return await _soundpool
            .load(await rootBundle.load("lib/sounds/cartoon_ascend.mp3"));
    }
  }

  Future<void> _endSound(int num) async {
    await new Future.delayed(const Duration(seconds: 1));
    print('end pressed:' + num.toString());
    setState(() {
      switch (num) {
        case 0:
          {
            _button0pressed = false;
            break;
          }
        case 1:
          {
            _button1pressed = false;
            break;
          }
        case 2:
          {
            _button2pressed = false;
            break;
          }
        case 3:
          {
            _button3pressed = false;
            break;
          }
        case 4:
          {
            _button4pressed = false;
            break;
          }
        case 5:
          {
            _button5pressed = false;
            break;
          }
        case 6:
          {
            _button6pressed = false;
            break;
          }
        case 7:
          {
            _button7pressed = false;
            break;
          }
        case 8:
          {
            _button8pressed = false;
            break;
          }
        case 9:
          {
            _button9pressed = false;
            break;
          }
      }
    });
  }

  Future<void> _playSound(int num) async {
    print('play sound...' + num.toString());
    switch (num) {
      case 0:
        {
          setState(() {
            _button0pressed = true;
          });
          _soundpool.play(await _soundId0);
          await _endSound(0);
          break;
        }
      case 1:
        {
          setState(() {
            _button1pressed = true;
          });
          _soundpool.play(await _soundId1);
          await _endSound(1);
          break;
        }
      case 2:
        {
          setState(() {
            _button2pressed = true;
          });
          _soundpool.play(await _soundId2);
          await _endSound(2);
          break;
        }
      case 3:
        {
          setState(() {
            _button3pressed = true;
          });
          _soundpool.play(await _soundId3);
          await _endSound(3);
          break;
        }
      case 4:
        {
          setState(() {
            _button4pressed = true;
          });
          _soundpool.play(await _soundId4);
          await _endSound(4);
          break;
        }
      case 5:
        {
          setState(() {
            _button5pressed = true;
          });
          _soundpool.play(await _soundId5);
          await _endSound(5);
          break;
        }
      case 6:
        {
          setState(() {
            _button6pressed = true;
          });
          _soundpool.play(await _soundId6);
          await _endSound(6);
          break;
        }
      case 7:
        {
          setState(() {
            _button7pressed = true;
          });
          _soundpool.play(await _soundId7);
          await _endSound(7);
          break;
        }
      case 8:
        {
          setState(() {
            _button8pressed = true;
          });
          _soundpool.play(await _soundId8);
          await _endSound(8);
          break;
        }
      case 9:
        {
          setState(() {
            _button9pressed = true;
          });
          _soundpool.play(await _soundId9);
          await _endSound(9);
          break;
        }

      default:
        {
          setState(() {
            _button1pressed = true;
          });
          _soundpool.play(await _soundId1);
          await _endSound(1);
          break;
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    setWidth(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(FontAwesome5Solid.history, color: Colors.white,size:
            25,),
            onPressed: (){
              _playSound(0);
            },
          ),
          SizedBox(width: 30,),
        ],
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    height: _hightwidth,
                    width: _hightwidth,
                    decoration: new BoxDecoration(
                        color: Colors.blue,
                        borderRadius:
                            new BorderRadius.all(Radius.circular(_radius)),
                        border: Border.all(
                            width: (_button1pressed) ? _br : 0,
                            color: _bordercolor)),
                    margin: EdgeInsets.all(_margin),
                    child: IconButton(
                      tooltip: 'Cartoon Ascend',
                      iconSize: _iconsize,
                      color: Colors.white,
                      icon: Icon(FontAwesome.codiepie),
                      onPressed: () {
                        _playSound(1);
                      },
                    ),
                  ),
                  Container(
                    height: _hightwidth,
                    width: _hightwidth,
                    decoration: new BoxDecoration(
                        color: Colors.deepPurple,
                        borderRadius:
                            new BorderRadius.all(Radius.circular(_radius)),
                        border: Border.all(
                            width: (_button2pressed) ? _br : 0,
                            color: _bordercolor)),
                    margin: EdgeInsets.all(_margin),
                    child: IconButton(
                      tooltip: 'Horror Stab',
                      iconSize: _iconsize,
                      color: Colors.white,
                      icon: Icon(FontAwesome5Brands.affiliatetheme),
                      onPressed: () {
                        _playSound(2);
                      },
                    ),
                  ),
                  Container(
                    height: _hightwidth,
                    width: _hightwidth,
                    decoration: new BoxDecoration(
                      color: Colors.green,
                      borderRadius:
                          new BorderRadius.all(Radius.circular(_radius)),
                        border: Border.all(
                            width: (_button3pressed) ? _br : 0,
                            color: _bordercolor)
                    ),
                    margin: EdgeInsets.all(_margin),
                    child: IconButton(
                      tooltip: 'Ring Short',
                      iconSize: _iconsize,
                      color: Colors.white,
                      icon: Icon(FontAwesome5Solid.concierge_bell),
                      onPressed: () {
                        _playSound(3);
                      },
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    height: _hightwidth,
                    width: _hightwidth,
                    decoration: new BoxDecoration(
                      color: Colors.red,
                      borderRadius:
                          new BorderRadius.all(Radius.circular(_radius)),
                        border: Border.all(
                            width: (_button4pressed) ? _br : 0,
                            color: _bordercolor)
                    ),
                    margin: EdgeInsets.all(_margin),
                    child: IconButton(
                      tooltip: 'Horror Hit',
                      iconSize: _iconsize,
                      color: Colors.white,
                      icon: Icon(FontAwesome.frown_o),
                      onPressed: () {
                        _playSound(4);
                      },
                    ),
                  ),
                  Container(
                    height: _hightwidth,
                    width: _hightwidth,
                    decoration: new BoxDecoration(
                      color: Colors.amber,
                      borderRadius:
                          new BorderRadius.all(Radius.circular(_radius)),
                        border: Border.all(
                            width: (_button5pressed) ? _br : 0,
                            color: _bordercolor)
                    ),
                    margin: EdgeInsets.all(_margin),
                    child: IconButton(
                      tooltip: 'Male Laugh',
                      iconSize: _iconsize,
                      color: Colors.white,
                      icon: Icon(FontAwesome5.grin),
                      onPressed: () {
                        _playSound(5);
                      },
                    ),
                  ),
                  Container(
                    height: _hightwidth,
                    width: _hightwidth,
                    decoration: new BoxDecoration(
                      color: Colors.pinkAccent,
                      borderRadius:
                          new BorderRadius.all(Radius.circular(_radius)),
                        border: Border.all(
                            width: (_button6pressed) ? _br : 0,
                            color: _bordercolor)
                    ),
                    margin: EdgeInsets.all(_margin),
                    child: IconButton(
                      tooltip: 'Phone',
                      iconSize: _iconsize,
                      color: Colors.white,
                      icon: Icon(FontAwesome5Solid.phone_volume),
                      onPressed: () {
                        _playSound(6);
                      },
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    height: _hightwidth,
                    width: _hightwidth,
                    decoration: new BoxDecoration(
                      color: Colors.black54,
                      borderRadius:
                          new BorderRadius.all(Radius.circular(_radius)),
                        border: Border.all(
                            width: (_button7pressed) ? _br : 0,
                            color: _bordercolor)
                    ),
                    margin: EdgeInsets.all(_margin),
                    child: IconButton(
                      tooltip: 'Flash',
                      iconSize: _iconsize,
                      color: Colors.white,
                      icon: Icon(FontAwesome.flash),
                      onPressed: () {
                        _playSound(7);
                      },
                    ),
                  ),
                  Container(
                    height: _hightwidth,
                    width: _hightwidth,
                    decoration: new BoxDecoration(
                      color: Colors.orange,
                      borderRadius:
                          new BorderRadius.all(Radius.circular(_radius)),
                        border: Border.all(
                            width: (_button8pressed) ? _br : 0,
                            color: _bordercolor)
                    ),
                    margin: EdgeInsets.all(_margin),
                    child: IconButton(
                      tooltip: 'Chimp',
                      iconSize: _iconsize,
                      color: Colors.white,
                      icon: Icon(FontAwesome5Brands.mailchimp),
                      onPressed: () {
                        _playSound(8);
                      },
                    ),
                  ),
                  Container(
                    height: _hightwidth,
                    width: _hightwidth,
                    decoration: new BoxDecoration(
                      color: Colors.cyan,
                      borderRadius:
                          new BorderRadius.all(Radius.circular(_radius)),
                        border: Border.all(
                            width: (_button9pressed) ? _br : 0,
                            color: _bordercolor)
                    ),
                    margin: EdgeInsets.all(_margin),
                    child: IconButton(
                      tooltip: 'Bullhorn',
                      iconSize: _iconsize,
                      color: Colors.white,
                      icon: Icon(FontAwesome5Solid.bullhorn),
                      onPressed: () {
                        _playSound(9);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
