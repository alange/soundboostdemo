# SoundBoost

A new Flutter project for Web, IOS and Android.

## Getting Started
### Flutter based Web Application 

This project is just a little Web and Mobile Sound Demo with some fancy sound effects.

<img src="appimage.png"
     alt="Soundboost"
     width="30%"
     style="float: left; margin-right: 10px;" />

To build the demo just run: flutter build web

you can change between two simple layouts:

green & blue

green: green header and simple icon buttons with no action response.
blue: blue header and a black circle border response by clicking the icon buttons.

to switch between both just toggling blueGreen in main.dart:

```
11 bool blueGreen = true;
```

To push to Cloudfoundry create a manifest.yml in build/web

like:
```
---
applications:
  - name: DevDaySounds
    memory: 128M
    disk_quota: 128M
    routes:
    - route: devdaysounds.cfapps.io
```

after creating the manifest just add this file:
```
touch Staticfile
```

and now just 
```
cf push
```


